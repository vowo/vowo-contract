pragma solidity ^0.4.24;

/// @title vowo contract which handles transport of goods
contract Vowo {
    // address of the contract owner, a trusted source in the real world
    address public owner;

    // productsCount to track all products and give them an id
    uint internal productsCount;

    // map of all claims, authorized organisations
    // this is basically just a map of the associated addresses
    mapping(address => bool) public claims;

    // map of all products associated with a claim
    mapping(address => mapping(uint => Product)) public products;

    // constructor which will be called while deploying the contract
    // owner is the contract creator, no need for transfering this right
    constructor() public {
        owner = msg.sender; 
    }

    // represent the product
    // it is not decided atm what this product is exactly
    struct Product {
        address claim;
        uint timestamp;
        uint logCount;
        // map of all products
        mapping(uint => Log) logs;
    }

    // represents a log with latlong data
    // https://en.wikipedia.org/wiki/Decimal_degrees
    struct Log {
        uint8 latInteger;
        uint8 latDecimal;
        uint8 longInteger;
        uint8 longDecimal;
        uint timestamp;
    }

    // create a claim
    function createClaim(address _claimAddress) external {
        // require that only the owner can create a new claim
        require(owner == msg.sender);
        // require that he claim is not set already
        require(claims[_claimAddress] == false);

        // create a new claim
        claims[_claimAddress] = true;
    }

    // update claim
    function updateClaim(address _newOwner, uint _productId) external {
        Product storage product = products[msg.sender][_productId];
        require(product.claim != address(0));

        product.claim = _newOwner;
    }

    // create a product
    function createProduct() external {
        // creates a new product and adds it to products
        Product memory product = Product(msg.sender, now, 0);
        products[msg.sender][productsCount] = product;
        productsCount++;
    }

    // create a product log
    function createLog(uint _productId, uint8 _latInteger, uint8 _latDecimal, uint8 _longInteger, uint8 _longDecimal) external {
        Product storage product = products[msg.sender][_productId];

        // create log
        Log memory log = Log(_latInteger, _latDecimal, _longInteger, _longDecimal, now);
        product.logs[product.logCount] = log;
        product.logCount++;
    }

    // get current amount of logs
    function getLogCount(uint _productId) external view returns (uint) {
        Product storage product = products[msg.sender][_productId];
        return product.logCount;
    }

    // get one log of a product
    // why: https://solidity.readthedocs.io/en/v0.5.0/frequently-asked-questions.html#is-it-possible-to-return-an-array-of-strings-string-from-a-solidity-function
    function getLog(uint _productId, uint _logId) external view returns (uint8, uint8, uint8, uint8, uint) {
        Product storage product = products[msg.sender][_productId];
        Log storage log = product.logs[_logId];
        return (log.latInteger, log.latDecimal, log.longInteger, log.longDecimal, log.timestamp);
    }
}