# vowo Truffle Box

`wip` - vowo example Truffle project

Contract Creation: https://ropsten.etherscan.io/tx/0x696d6ac7f9470b1ec29f2dd10885eb6af6c55dc4070c0bd32e8e9b51f54716c2 </br>
Contract Address: https://ropsten.etherscan.io/address/0x83dccc9f44d9ccf35d5cac254cf6245542e6fdfa

Deploy on Ropsten: https://medium.com/swlh/deploy-smart-contracts-on-ropsten-testnet-through-ethereum-remix-233cd1494b4b

## Contract Setup

Compile and migrate Smart Contract:

```bash
truffle compile
truffle migrate
```

Or if not migrating the first time:

```bash
truffle migrate --reset
```

## Truffle Console

Open up the console:

```bash
truffle console
```

Get contract instance and check if it worked:

```bash
Vowo.deployed().then(function(instance) {app=instance})
app.owner()
```

Create a Claim:

```bash
app.createClaim(0xC01F4884649413646eD6fc90C56E4cACeb1947C8, {from: web3.eth.accounts[0]})
```

Create a Product:

```bash
app.createProduct({from: web3.eth.accounts[1]})
```

Create a Log:

```bash
app.createLog(0, 46, 932882, 7, 103963, {from: web3.eth.accounts[1]})
```

Get a Log-Count:

```bash
app.getLogCount(0, {from: web3.eth.accounts[1]}).then(function(l) {log = l})
log.toNumber()
```

Get a Log:

```bash
app.getLog(0, 0, {from: web3.eth.accounts[1]}).then(function(i) {log = l})
log
log[0].toNumber()
```